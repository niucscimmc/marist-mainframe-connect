﻿namespace Marist_Mainframe_Connect
{
    partial class aboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(aboutForm));
            this.infoLbl = new System.Windows.Forms.Label();
            this.licenseLbl = new System.Windows.Forms.Label();
            this.licenseTxtBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // infoLbl
            // 
            this.infoLbl.AutoSize = true;
            this.infoLbl.Location = new System.Drawing.Point(13, 13);
            this.infoLbl.Name = "infoLbl";
            this.infoLbl.Size = new System.Drawing.Size(209, 78);
            this.infoLbl.TabIndex = 0;
            this.infoLbl.Text = "Marist Mainframe Connect V1.00\r\nProgrammed by Jozef Kenar\r\nFebruary 2014\r\n\r\nCreat" +
    "ed using Microsoft Visual Studio 2013\r\nC# .NET";
            // 
            // licenseLbl
            // 
            this.licenseLbl.AutoSize = true;
            this.licenseLbl.Location = new System.Drawing.Point(13, 109);
            this.licenseLbl.Name = "licenseLbl";
            this.licenseLbl.Size = new System.Drawing.Size(99, 13);
            this.licenseLbl.TabIndex = 1;
            this.licenseLbl.Text = "License Information";
            // 
            // licenseTxtBox
            // 
            this.licenseTxtBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.licenseTxtBox.Location = new System.Drawing.Point(16, 139);
            this.licenseTxtBox.Multiline = true;
            this.licenseTxtBox.Name = "licenseTxtBox";
            this.licenseTxtBox.ReadOnly = true;
            this.licenseTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.licenseTxtBox.Size = new System.Drawing.Size(387, 207);
            this.licenseTxtBox.TabIndex = 2;
            this.licenseTxtBox.Text = resources.GetString("licenseTxtBox.Text");
            // 
            // aboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 358);
            this.Controls.Add(this.licenseTxtBox);
            this.Controls.Add(this.licenseLbl);
            this.Controls.Add(this.infoLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "aboutForm";
            this.Text = "About MMC 1.0";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label infoLbl;
        private System.Windows.Forms.Label licenseLbl;
        private System.Windows.Forms.TextBox licenseTxtBox;
    }
}