﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Marist_Mainframe_Connect
{
    public partial class MaristMainframeConnectForm : Form
    {
        public MaristMainframeConnectForm()
        {
            InitializeComponent();
            getMaristOutputTab.Enabled = false;
            
        }

        private void newConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void aboutMMCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using(aboutForm aF = new aboutForm())
            {
                aF.ShowDialog();
            }
        }


    }
}
